name             'marketo-tools'
maintainer       'Jacob Vosmaer'
license          'MIT'
description      'Installs/Configures marketo-tools'
long_description 'Installs/Configures marketo-tools'
version          '0.1.2'

depends 'gitlab-vault'
