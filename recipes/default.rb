include_recipe 'gitlab-vault'

include_recipe 'marketo-tools::ruby'
include_recipe 'marketo-tools::nginx'

marketo_tools_conf = GitLab::Vault.get(node, 'marketo-tools')

username = 'gitlab-mrkt'
homedir = '/home/gitlab-mrkt'
repo = 'https://ops.gitlab.net/gitlab-com/marketo-tools.git'

package 'git'

user username do
  manage_home true
  home homedir
end

checkout_dir = marketo_tools_conf['install_dir']

directory checkout_dir do
  owner username
  group username
end

git checkout_dir do
  repository repo
  user username
  group username
  notifies :run, 'bash[bundle install]'
  notifies :run, 'execute[restart unicorn]'
end

bash 'bundle install' do
  code <<-EOS
set -e
cd #{checkout_dir}
bundle install --path #{homedir}/.bundle
EOS
  user username
  group username
  action :nothing
end

execute 'restart unicorn' do
  command 'sv restart mrkt-unicorn'
  action :nothing
end

template File.join(checkout_dir, '.env.production') do
  source 'env.production.erb'
  mode 0400
  owner username
  variables(
    env: marketo_tools_conf['env'],
  )
  notifies :run, 'execute[restart unicorn]'
end

unicorn_rb_path = File.join(checkout_dir, 'unicorn.rb')

template unicorn_rb_path do
  variables(
    socket_path: marketo_tools_conf['unicorn_socket_path'],
    worker_processes: marketo_tools_conf['unicorn_worker_processes'],
  )
  notifies :run, 'execute[restart unicorn]'
end

package 'runit'

unicorn_svc_dir = '/etc/service/mrkt-unicorn'

directory unicorn_svc_dir

template File.join(unicorn_svc_dir, 'run') do
  source 'sv-mrkt-unicorn-run.erb'
  mode 0755
  variables(
    user: username,
    app_dir: checkout_dir,
    unicorn_rb_path: unicorn_rb_path,
  )

  notifies :run, 'execute[restart unicorn]'
end

unicorn_log_dir = '/var/log/mrkt-unicorn'

directory unicorn_log_dir

unicorn_log_svc_dir = File.join(unicorn_svc_dir, 'log')

directory unicorn_log_svc_dir

template File.join(unicorn_log_svc_dir, 'run') do
  source 'sv-mrkt-unicorn-log-run.erb'
  mode 0755
  variables(
    log_dir: unicorn_log_dir,
  )
end
