# cookbook-marketo-tools-cookbook

**Archived**: This cookbook is no longer used. See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/2331

This cookbook deploys the master branch of
https://gitlab.com/gitlab-com/marketo-tools/ behind NGINX and HTTPS.
Because Chef runs every ~30 minutes in production, this means changes
on master in the marketo-tools repo will get deployed automatically.
If you do not want to wait for that run `sudo chef-client` on
marketo-tools.gitlap.com.

Further details: we install Ruby in recipes/ruby.rb. The marketo-tools
Rack app is served by Unicorn, which is managed by Runit.

TODO: Enter the cookbook description here.

## Supported Platforms

TODO: List your supported platforms.

## Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['cookbook-marketo-tools']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### cookbook-marketo-tools::default

Include `cookbook-marketo-tools` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[cookbook-marketo-tools::default]"
  ]
}
```

## License and Authors

Author:: YOUR_NAME (<YOUR_EMAIL>)
